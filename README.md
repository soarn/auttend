# Auttend

Auttend automagically signs you in with RapidIdentity and submits pre-filled google forms allowing you to sign into all of your classes in less than 30 seconds, all without needing to touch anything.

## Installation

```shell
> git clone https://gitlab.com/Osnott/auttend/-/tree/master
```
or
```shell
> curl https://ghostbin.co/paste/pa2ph/raw -o auttend.py
```

Auttend also requires a JSON file with pre-filled google form links, you can find how to do that [here](https://support.google.com/docs/thread/3341555?hl=en&msgid=14432964). I'm currently working on something to explain this better and possibly even a tool to make them easily.

The JSON file should look something like this

```JSON
// urls.json
{
    "urls": {
        "1": {
            "url": "https://docs.google.com/forms..."
        },
        "2": {
            "url": "https://docs.google.com/forms..."
        },
        "3": {
            "url": "https://docs.google.com/forms..."
        },
        "4": {
            "url": "https://docs.google.com/forms..."
        },
        "6": {
            "url": "https://docs.google.com/forms..."
        },
        "7": {
            "url": "https://docs.google.com/forms..."
        }
    }
}
```

where the object is the period number and the url is the pre-filled google form link

## Usage

```
auttend.py [-h] -s ID -p PASS [-i URL] [-c CHROMEDRIVER] [-t]

-h, --help                                   show help message and exit
-s ID, --id ID, --sid ID                     your student ID, preceded by an *s*
-p PASS, --pass PASS, --password PASS        your password (don't worry, we never store your password)
-i URL, --url URL, --json URL                the file path for the JSON file containing the urls. By default, the program assumes it is in the current directory. Refer to [link] to see how to make your urls.
-c CHROMEDRIVER, --chromedriver CHROMEDRIVER the path to chromedriver. By default, the program assumes it is in your home directory.
-t, --test                                   disables submitting the forms at the end.
```

## Dependencies

* [Webbot](https://github.com/nateshmbhat/webbot)
* [ChromeDriver](https://chromedriver.chromium.org/downloads)
