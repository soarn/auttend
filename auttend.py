# Made with <3 by Aiden Onstott

import webbot
import json
import time
import argparse
from pathlib import Path


def main():
    # Argparse stuff to make things friendly (and also not store the
    # password in plaintext)
    parser = argparse.ArgumentParser('auttend.py')
    parser.add_argument('-s', '--id', '--sid',
                        required=True, help='your student ID, '
                        'preceded by an *s*')
    parser.add_argument('-p', '--pass', '--password', required=True,
                        help='your password (don\'t worry, we never '
                        'store your password)')
    parser.add_argument('-i', '--url', '--json', required=False,
                        help='the file path for the JSON file containing the '
                        'urls. By default, the program assumes it is in the '
                        'current directory. Refer to the README to see how '
                        'to make your urls.')
    parser.add_argument('-c', '--chromedriver', required=False,
                        help='the path to chromedriver. By default, the'
                        'program assumes it is in your home directory.')
    parser.add_argument('-t', '--test', required=False, action='store_false',
                        help='disables submitting the forms at the end.')

    args = vars(parser.parse_args())

    # Open JSON file
    if args['url'] is not None:
        with open(args['url'], 'r') as url_file:
            data = url_file.read()
    else:
        with open('urls.json', 'r') as url_file:
            data = url_file.read()

    urls = json.loads(data)

    # Start chromedriver
    if args['chromedriver'] is not None:
        web = webbot.Browser(showWindow=True,
                             executable_path=args['chromedriver'])
    else:
        web = webbot.Browser(showWindow=True,
                             executable_path=str(Path.home()) +
                             r'\chromedriver.exe')

    # Open urls and log sign into all google forms
    for url in urls['urls']:
        web.go_to(urls['urls'][url]['url'])
        if url == "1":
            web.type(args['id'] + '@stu.tempeunion.org')
            web.press(web.Key.ENTER)
            time.sleep(1)
            web.type(args['id'], into='Username')
            web.type(args['pass'], into='Password', id='ember465')
            time.sleep(2)
            web.click("Go", id='button', classname='authn-go-button')
            time.sleep(3)
            web.click(id='button', classname='VfPpkd-LgbsSe VfPpkd-LgbsSe-'
                      'OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe'
                      'DuMIQc qIypjc TrZEUc')
            time.sleep(3)
        if args['test']:
            web.click('Submit', tag='span')
            print("Submitted period " + url)
        time.sleep(2)

    print('Have a good day at school!')


main()
